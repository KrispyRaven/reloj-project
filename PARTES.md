#Partes

IC LM8560



Center tap transformer (6 or 9v output)

https://www.digikey.com/en/products/detail/zettler-magnetics/BV301S09020-ZU/12093239

Capacitor 470uF 16V

https://www.crcibernetica.com/470uf-electrolytic-capacitor/

Capacitor 1nF

https://www.crcibernetica.com/ceramic-capacitor-0-1-0-01-0-001-uf-50v/

Capacitor 15nF

https://www.microjpm.com/products/a0-015uf-50v-capacitor-ceramico/

LED (59pcs)

Led 3mm (56 verdes , 3 de otro color)
Diode 1N4007 (4pcs)

https://www.crcibernetica.com/general-purpose-rectifier-diode-1n4007/

Push button switch (5pcs)

https://www.crcibernetica.com/mini-push-button-switch/

Dip switch

https://www.digikey.com/en/products/detail/cui-devices/DS01-254-L-01BE/11310875

Terminal 2P

https://www.microjpm.com/products/bloque-terminal-de-tornillo-2-3-polos-3-5mm-y-5mm-distancia-entre-polos/

Piezo buzzer

https://www.crcibernetica.com/active-buzzer-pc-mount/

Resistor 100R 0.5W (2pcs)

https://www.crcibernetica.com/carbon-resistor-1-2w-5-10-pack/

Resistor 110K
Resistor 2.2K
Resistor 100K

https://www.crcibernetica.com/100k-ohm-resistor-10-pack/

Battery 9V With holder 

https://www.crcibernetica.com/9v-battery-holder-with-cover-and-switch/  (OPCIONAL)
