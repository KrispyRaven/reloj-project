# Clock Project

## Description
El objetivo principal de este proyecto es implementar el diseño de una pcb. Para esto se usara como base un circuito de un Reloj digital que emplea el integrado LM8650

## Visuals
Visuals if needed

## Usage
Este tendra sus botones respectivos para "setear" la alarma y hora. Ademas de tener interruptores para poder modificar como se muestra el formato de la fecha, ya sea 24/12 horas.

## Support
noelalberto98@gmail.com

## Road map
Ya se logro la implementacion del circuito del reloj. Ahaorita lo que se esta investigando es acerca de las protecciones contra sobre voltaje.

### Future implementations:
  * Proteccion sobre voltaje

## Authors and acknowledgment
  * Noel Blandon (noelalberto98@gmail.com)
  * Carmen

## License
This project is licensed under GPLv3+ (GNU General Public License version 3 or later).

## Project status
Active (2023.october.11)
