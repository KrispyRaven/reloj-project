# Condiciones de Operacion

## Funcionalidad esperada
La tarjeta trabajara como un reloj digital con sus respectivas funbciones para "setear" la hora y una alarma. Ademas de tener una opcion para cambiar el formato de como se muestra la hora, ya sea 12/24. 

## Condiciones eléctricas
La pcb trabajara con 2 tipos de fuentes, una principal y una secundaria. La principal conectada a un toma, este alimentara al transformador siendo su salida 9V, a su vez este alimentara a el set de leds y al integrado. Ademas se alimenta al integrado con una bateria de 9V, esta bateria solo mantentra activo a el modulo RTC del integrado si hay una desconexion de la fuente principal.

## Condiciones ambientales y mecánicas
* La humedad a la que se vera expuesto sera dependiendo al espacio en el que se va a mostrar el reloj, idealmente es para interiores debido a su tamaño. Y más especificamente en habitaciones del hogar o espacios de oficina. Entonces podemos tomar como despreciables las condiciones de humedad.
* Su tamaño no debe ser excesivamente grande ya que se planea usar en espacios como una mesa, las cuales hay mas objetos alrededor. Entonces no debe estorbar con las demas cosas del entorno. Por ello entre mas reducido sea su tamaño mejor. Exceptuando por su tamaño del arreglo de leds que muestra la informacion.
* Se plantea montar sobre alguna lamina de policarbonato y una lamina protectora transparente en frente. 
* No va a tener conectores externos debido a que el cable de conexion va a ser fijo

## Protecciones necesarias
Se va a implementar una proteccion contra sobre corrientes debido a que hay un mayor riesgo debido a que su fuente principal la corriente que se puede obtener directo de un toma. Esta proteccion se colocara en la pura entrada de la pcb con un fusible.